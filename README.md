# MathQuiz

MathQuiz web project for #JustCode 2018.

A web project that displays a math quiz consisting of addition, subtraction, multiplication and division problems with a page to show scores.