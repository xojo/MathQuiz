#tag Class
Protected Class App
Inherits WebApplication
	#tag Event
		Sub Open(args() as String)
		  Randomizer = New Random
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		Randomizer As Random
	#tag EndProperty


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
